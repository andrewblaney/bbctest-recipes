## Recipe Technical Test

### Quick Start
- Install the applications dependencies `npm i`
- `npm start`

### Structure
- The backend is a simple express server (server.js) that serves the JSON file as an API.
- The React app is scaffolded with [create-react-app](https://github.com/facebookincubator/create-react-app) which allowed
me to use the dev server and webpack hot reload config for development.

### Packages Uses
- react-scripts for compile / dev tasks
- express.js for server (see server.js)
- react, react-dom, react-router-dom for react stack
- axios used for http get