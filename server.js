// server.js
// Very simple Express app to server recipes.json as an API.
// usage: `node server.js` or `npm start`

let express = require('express'),
    app = express(),
    router = express.Router(),
    data = require('./public/recipes.json'),
    port = 3001;

// ignore CORS for local dev
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// index route returns all data in recipes.json
router.get('/', (req, res) => {
    res.json(data);
});

app.use('/', router);
app.listen(port);

console.log(`Server running on port ${port}.`);
