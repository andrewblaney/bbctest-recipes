// style & css
import './App.css';
import logo from './logo.svg';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import RecipeList from './components/RecipeList';
import RecipeDetail from './components/RecipeDetail';
import axios from 'axios';
import SearchBar from "./components/SearchBar";
import EmptyResultsMessage from "./components/EmptyResultsMessage";
import NoMatchingResultsMessage from "./components/NoMatchingResultsMessage";

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            recipes: [],
            filtered: [],
            searchString: ''
        }
    }

    // fetch data from our makeshift API
    componentDidMount() {
        axios.get('http://localhost:3001')
            .then(res => {
                let count = 0,
                    recipes = res.data.recipes.map(recipe => {
                        recipe.id = ++count;
                        return recipe;
                    });
                this.setState({ recipes, filtered: recipes });
            });
    }

    // function called by SearchBar to filter recipes
    filterResults = (string) => {
        let recipes = this.state.recipes.filter((recipe) => {
            // first check for match in recipe name
            return recipe.name.toLowerCase().indexOf(string.toLowerCase()) !== -1 ||
                // dirty loop over ingredients to see if search is found
                (recipe.ingredients.filter((i) => {
                    return i.toLowerCase().indexOf(string.toLowerCase()) > -1;

                }).length > 0);
        });

        this.setState({
            filtered: recipes, // save the filtered results
            searchString: string // save the string we searched
        });
    };

    render() {
        // conditionals for displaying error messages
        let emptyResponse = !this.state.filtered.length && !this.state.recipes.length,
            noMatch = !this.state.filtered.length && this.state.recipes.length;


        return (
            <Router>
                <div className="App">
                    <div className="App-header">
                        <img src={logo} className="App-logo" alt="logo" />
                        <h2>Hi Joe, here are your recipes.</h2>
                    </div>

                    <Route exact={true} path="/" render={ () => (
                        <div>
                            <SearchBar filterResults={this.filterResults} searchString={this.state.searchString}/>
                            <RecipeList recipes={this.state.filtered}/>
                            {noMatch && <NoMatchingResultsMessage />}
                            {emptyResponse && <EmptyResultsMessage />}
                        </div>
                    )} />

                    <Route path="/r/:recipeId" render={( match ) => {
                        let recipe = this.state.recipes.find((i) => {
                            return i.id === parseInt(match.match.params.recipeId, 10);
                        });

                        return (
                            <RecipeDetail recipe={recipe}/>
                        );
                    }}/>

                </div>
            </Router>
        );
    }
}

export default App;
