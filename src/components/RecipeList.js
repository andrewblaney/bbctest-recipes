import React from 'react';
import Recipe from "./Recipe";

/**
 * <RecipeList recipes={recipes} />
 *
 * Component that displays a list of <RecipeCard> components for each recipe in the passed array.
 *
 */
class RecipeList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            recipes: props.recipes || [],
            filtered: props.recipes || []
        };
    }

    // to fill from api response
    componentWillReceiveProps(nextProps) {
        this.setState({
            recipes: nextProps.recipes,
            filtered: nextProps.recipes
        });
    }

    // map recipes to a RecipeCard component
    renderRecipes() {
        return this.state.filtered.map((recipe) =>
            <Recipe key={recipe.id} {...recipe} toggleFavourite={this.toggleFavourite}/>
        );
    }

    // beginning of favourite feature
    toggleFavourite = (id) => {
        let recipes = this.props.recipes.map(recipe =>  {
            if (id === recipe.id) {
                recipe.favourited = !recipe.favourited;
            }
            return recipe;
        });

        this.setState({ recipes });
    };

    render() {

        let className = 'RecipeList ' + (this.state.filtered.length ? '' : 'hidden');

        return (
            <div className={className}>
                <table className="RecipeList-table">
                    <thead className="RecipeListHeader">
                    <tr>
                        <th>Title</th>
                        <th>Cooking Time</th>
                        <th>Main Ingredients</th>
                        <th>Favourite</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderRecipes()}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default RecipeList;