import React from 'react';
import { Link } from 'react-router-dom';

/**
 * <Recipe {..recipe} />
 *
 * Component to display a table row of details for a single Recipe
 */
class Recipe extends React.Component {

    renderIngredients() {
        return this.props.ingredients.map((item, index) =>
            <li key={index}>{item}</li>
        );
    }

    handleToggleFavourite = () => {
        this.props.toggleFavourite(this.props.id);
    };

    render() {
        return (
            <tr className="RecipeCard">
                <td>
                    <Link to={`/r/${this.props.id}`} data={this.props}>
                        {this.props.name}
                    </Link>
                </td>
                <td>
                    {this.props.cookingTime}
                </td>
                <td>
                    <ul className="ingredients-list">
                        {this.renderIngredients()}
                    </ul>
                </td>
                <td>
                    <div className="favouritefavourite-marker">
                        {this.props.favourited && 'x'}
                    </div>
                </td>
                <td>
                    <div className="favourite-toggle" onClick={this.handleToggleFavourite}>
                        <button>{!this.props.favourited && 'Add Favourite'} {this.props.favourited && 'Remove Favourite'}</button>
                    </div>
                </td>

            </tr>
        );
    }
}

export default Recipe;