import React from 'react';

/**
 * <NoMatchingResultsMessage />
 *
 * Used when filter query produces no results.
 */
class NoMatchingResultsMessage extends React.Component {

    render() {
        return (
            <div className="NoMatchingResultsMessage">
                <h1>Sorry, nothing matched your filter term</h1>
            </div>
        );
    }
}

export default NoMatchingResultsMessage;