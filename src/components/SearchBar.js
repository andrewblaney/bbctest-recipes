/**
 * Created by andrewblaney on 20/09/2017.
 */
import React from 'react';

/**
 * <SearchBar searchString={searchString} />
 *
 * Component bound to a searchString variable in parent state so that search filters are remembered whilst navigating.
 */
class SearchBar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            searchString: props.searchString
        }
    }

    handleChange = (e) => {
        this.props.filterResults(e.target.value);
        this.setState({ searchString: e.target.value })
    };


    render() {
        return (
            <div className="SearchBar">
                <input type="text" value={this.state.searchString} placeholder="Filter Recipes.." onChange={this.handleChange}/>
            </div>
        );
    }
}

export default SearchBar;