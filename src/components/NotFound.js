import React from 'react';

/**
 * <NotFound />
 *
 * Displays a message when an invalid recipe ID is given to Router.
 */
class NotFound extends React.Component {

    render() {
        return (
            <div className="NotFound">
                <h1>Sorry, this recipe doesn't exist or may have been removed</h1>
            </div>
        );
    }
}

export default NotFound;