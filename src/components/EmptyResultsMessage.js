import React from 'react';

/**
 * <EmptyResultsMessage />
 *
 * Displays a message for when no items returned from the API.
 */
class EmptyResultsMessage extends React.Component {

    render() {
        return (
            <div className="EmptyResultsMessage">
                <h1>Sorry, we currently have no recipes for you</h1>
            </div>
        );
    }
}

export default EmptyResultsMessage;