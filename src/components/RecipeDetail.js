import React from 'react';
import { Link } from 'react-router-dom';
import NotFound from "./NotFound";


/**
 * <RecipeDetail recipe={recipe} />
 *
 * Component to display a table row of details for a single Recipe
 */
class RecipeDetail extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            recipe: props.recipe || {}
        }

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            recipe: nextProps.recipe,
        });
    }

    renderIngredients() {
        if (this.state.recipe.hasOwnProperty('ingredients')) {
            return this.state.recipe.ingredients.map((item, index) =>
                <li key={index}>{item}</li>
            );
        }
    }

    render() {

        let content = this.state.recipe ?
            (
                <div>
                    <h1>{this.state.recipe.name}</h1>
                    <img src={this.state.recipe.imageUrl} alt={this.state.recipe.name}/>
                    <h4>Cooking time: {this.state.recipe.cookingTime}</h4>
                    <div>
                        <ul className="ingredients-list">
                            {this.renderIngredients()}
                        </ul>
                    </div>
                </div>
            )
            : ( <NotFound/> );

        return (
            <div>
                <Link to="/"><button className="home-button">Home</button></Link>
                {content}
            </div>
        );
    }
}

export default RecipeDetail;